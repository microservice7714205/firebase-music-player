import React, { useEffect, useRef, useState } from "react";
import firebase from "firebase/app";

import "firebase/firestore";
import "firebase/auth";
import "firebase/analytics";

import "firebase/storage";
import FireBase from "./init";
const storage = FireBase.storage();
const firestore = FireBase.firestore();

function ListMusic() {
  let [musicData, setMusicData] = useState([]);
  let getAllMusic = async () => {
    const fileRef = await storage.ref().child("").listAll();
    let data = [];
    data = fileRef.items.map((ref) => {
      return {
        name: ref.name,
      };
    });

    setMusicData(data);
  };

  useEffect(() => {
    getAllMusic();
  }, []);

  const [songName, setSongName] = useState("");
  const [songUrl, setSongUrl] = useState("");
  const [audio, setAudio] = useState(null);

  useEffect(() => {
    let audio = new Audio(songUrl);
    setAudio(audio);
  }, [songUrl]);

  useEffect(() => {
    if (audio) {
      audio.play();
    }
  }, [audio]);

  const playSong = (songName, playState) => {
    storage
      .ref()
      .child(songName)
      .getDownloadURL()
      .then((url) => {
        setSongUrl(url);
      });
  };

  useEffect(() => {
    if (songName) {
      playSong(songName, true);
    }
  }, [songName]);
  return (
    <>
      {musicData.map((music, index) => {
        return (
          <span
            key={index}
            style={{ color: "white", cursor: "pointer" }}
            onClick={() => {
              if (music.name !== songName) {
                setSongName(music.name);
                audio.pause();
                setAudio(null);
              } else {
                if (!audio.paused) {
                  audio.pause();
                } else {
                  audio.play();
                }
              }
            }}
          >
            {music.name}
          </span>
        );
      })}
    </>
  );
}

export default ListMusic;
