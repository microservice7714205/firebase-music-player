import React, { useEffect, useRef, useState } from "react";
import "./App.css";

import "firebase/firestore";
import "firebase/auth";
import "firebase/analytics";

import "firebase/storage";
import ListMusic from "./ListMusic";
import UploadAudio from "./UploadAudio";

function App() {
  useEffect(() => {
    (async () => {})();
  }, []);

  const BR_tags = [];
  for (let index = 0; index < 8; index++) {
    BR_tags.push(<br />);
  }

  return (
    <div className="App">
      {BR_tags}

      <UploadAudio />

      <ListMusic />
    </div>
  );
}

export default App;
