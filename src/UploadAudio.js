import React, { useEffect, useRef, useState } from "react";
import firebase from "firebase/app";
import "firebase/firestore";
import "firebase/auth";
import "firebase/analytics";

import "firebase/storage";
import ClipLoader from "react-spinners/ClipLoader";
import FireBase from "./init";

const storage = FireBase.storage();
const firestore = FireBase.firestore();

function UploadAudio() {
  const [audio, setaudio] = useState("");
  const [showLoader, setShowLoader] = useState(false);

  const upload = () => {
    if (audio == null) return;

    setShowLoader(true);
    storage
      .ref(`${audio.name}`)
      .put(audio)
      .on(
        "state_changed",
        (snapshot) => {
          var progress =
            (snapshot.bytesTransferred / snapshot.totalBytes) * 100;
          console.log("Upload is " + progress + "% done");
          switch (snapshot.state) {
            case FireBase.storage.TaskState.PAUSED: // or 'paused'
              console.log("Upload is paused");
              break;
            case FireBase.storage.TaskState.RUNNING: // or 'running'
              console.log("Upload is running");
              break;
          }
        },
        alert,
        () => {
          setShowLoader(false);
          alert("Upload successful!!");
        }
      );
  };

  return (
    <div className="App">
      <center>
        <input
          type="file"
          accept="audio/*"
          onChange={(e) => {
            setaudio(e.target.files[0]);
          }}
        />
        <button onClick={upload}>Upload</button>
      </center>
      {showLoader && (
        <ClipLoader
          color="#ffffff"
          loading={true}
          size={150}
          aria-label="Loading Spinner"
          data-testid="loader"
        />
      )}
    </div>
  );
}

export default UploadAudio;
